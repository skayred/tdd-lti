class AddDebugToSubmission < ActiveRecord::Migration[5.2]
  def change
    add_column :submissions, :debug, :text
  end
end
