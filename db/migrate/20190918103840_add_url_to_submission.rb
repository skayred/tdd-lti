class AddUrlToSubmission < ActiveRecord::Migration[5.2]
  def change
    add_column :submissions, :url, :string
  end
end
