#!/bin/bash

argc=$#
argv=($@)

UUID=$1
USER_ID=$2
PORT=$3
SOURCE_REP=$4

mkdir reps
cd reps
mkdir videos
mkdir origin$UUID
mkdir origin$UUID/cypress
mkdir origin$UUID/cypress/integration
for (( j=4; j<argc; j++ )); do
  git clone ${argv[j]} tests$j$UUID
  yes | cp -rf ./tests$j$UUID/cypress/integration/* ./origin$UUID/cypress/integration/
done
cd origin$UUID

echo "{\"baseUrl\": \"$SOURCE_REP\"}" > cypress.json

npm init -y
npm install --save-dev cypress mocha mocha-spec-json-reporter
npx cypress run --reporter mocha-spec-json-reporter > ../../log/output$PORT.log 2>&1
cp ./mocha-output.json ../../log/mocha-output$PORT.json
cp ./cypress/videos/*.mp4 ../../reps/videos/$USER_ID.mp4

cd ..
rm -rf ./origin$UUID
rm -rf ./tests*$UUID
