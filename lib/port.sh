BASE_PORT=$(shuf -i 10000-50000 -n 1)
INCREMENT=3

port=$BASE_PORT
isfree=$(netstat -taln | grep $port)

while [[ -n "$isfree" ]]; do
    port=$(shuf -i 10000-50000 -n 1)
    isfree=$(netstat -taln | grep $port)
done

echo $port