#!/bin/bash

argc=$#
argv=($@)

UUID=$1
USER_ID=$2
PORT=$3
SOURCE_REP=$4

mkdir reps
cd reps
mkdir videos
git clone $SOURCE_REP origin$UUID
mkdir origin$UUID/cypress
mkdir origin$UUID/cypress/integration
for (( j=4; j<argc; j++ )); do
  git clone ${argv[j]} tests$j$UUID
  yes | cp -rf ./tests$j$UUID/cypress/integration/* ./origin$UUID/cypress/integration/
done
cd origin$UUID
mkdir docker
cp ../../lib/Dockerfile docker/
cp ../../lib/start2.sh docker/

docker rmi PORT
echo "{\"baseUrl\": \"http://localhost:$PORT/\"}" > cypress.json
docker build -t $PORT -f docker/Dockerfile .
echo 'Built!'
DOCKER_PID="$(docker run -d -p $PORT:80 --name $PORT --rm $PORT)"
echo 'Run!'

npm install --save-dev cypress mocha mocha-spec-json-reporter
npx cypress run --reporter mocha-spec-json-reporter > ../../log/output$PORT.log 2>&1
cp ./mocha-output.json ../../log/mocha-output$PORT.json
cp ./cypress/videos/*.mp4 ../../public/videos/$USER_ID.mp4

docker kill $PORT
docker rmi $PORT

cd ..
rm -rf ./origin$UUID
rm -rf ./tests*$UUID
