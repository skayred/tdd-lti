class SubmissionsController < ApplicationController
  before_action :admin_only

  def index
    if params[:uid] && !params[:uid].empty?
      @subs = Submission.where(user_id: params[:uid])
    elsif params[:assignment] && !params[:assignment].empty?
      @subs = Submission.where(assignment_id: params[:assignment])
    elsif params[:same] && !params[:same].empty?
      @subs = Submission.where(url: Submission.select(:url).group(:url).having("count(*) > 1").select(:url))
    else
      @subs = Submission.all
    end
    @subs = @subs.order('created_at DESC').paginate(page: params[:page])
    @asses = Assignment.all
  end

  def show
    @sub = Submission.find_by_id(params[:id])

    render :show
  end
  
  private
 
  def admin_only
    unless current_user.admin?
      flash[:error] = "You must be an admin to access this information"
      redirect_to root_url
    end
  end
end
